'use strict'
module.exports = {
  NODE_ENV: '"production"',
  baseUrl: `\`${process.env.BASE_URL}\`` || '"http://anteco.parilova.com"'
}
