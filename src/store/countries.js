// import {CATEGORIES} from '../constants'
const removeLink = (items) => {
  return items.map((c) => ({
    ...c,
    link: c.link.replace(process.env.baseUrl, '')
  }))
}
export default {
  state: {
    continents: [],
    countries: []
  },
  getters: {
    getContinentBySlug: ({continents}) => (slug) => {
      return continents.find((continent) => continent.slug === slug)
    },
    getCountryBySlug: ({countries}) => (slug) => {
      return countries.find((contry) => contry.slug === slug)
    },
    getCountries: ({countries}) => (continentId) => {
      return countries.filter(({acf}) => acf.continent.ID === continentId)
    }
  },
  actions: {
    init ({dispatch}) {
      dispatch('requestCountries')
      dispatch('requestContinents')
    },
    requestCountries ({dispatch, commit}) {
      dispatch('requestItemsByType', {name: 'country'}, {root: true})
        .then(({data}) => commit('SET_COUNTRIES', data))
    },
    requestContinents ({dispatch, commit}) {
      dispatch('requestItemsByType', {name: 'continent'}, {root: true})
        .then(({data}) => commit('SET_CONTINENTS', data))
    }
  },
  mutations: {
    SET_COUNTRIES: (state, countries) => {
      state.countries = removeLink(countries)
    },
    SET_CONTINENTS: (state, continents) => {
      state.continents = removeLink(continents)
    }
  },
  namespaced: true
}
