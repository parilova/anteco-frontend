// import {CATEGORIES} from '../constants'

export default {
  state: {
    hotels: []
  },
  getters: {
    getHotelsByCountryId: ({hotels}) => (countryId) => {
      return hotels.filter((hotel) => hotel.acf.country.ID === countryId)
    }
  },
  actions: {
    requestHotels: async ({commit, dispatch}) => {
      const hotels = await dispatch('get', ['/wp-json/wp/v2/hotel'], {root: true})
        .then(({data}) => data)
      commit('SET_HOTELS', hotels)
      return hotels
    }
  },
  mutations: {
    SET_HOTELS: (state, hotels) => {
      state.hotels = hotels
    }
  },
  namespaced: true
}
