import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
// import querystring from 'querystring'
import countriesModule from './countries'
import toursModule from './tour'
import hotelsModule from './hotel'
import modalModule from './modal'
import postsModule from './posts'
import categoriesModule from './categories'
import photoGalleryModule from './photoGallery'

Vue.use(Vuex)

function getImagesForPost ({acf}) {
  return Object.keys(acf)
    .filter((key) => key.includes('image'))
    .map((key) => acf[key])
    .filter((val) => val)
}

function getImagesForPosts (posts) {
  return posts.map(getImagesForPost).reduce((acc, item) => [...acc, ...item], [])
}

export default new Vuex.Store({
  modules: {
    countriesModule,
    toursModule,
    hotelsModule,
    modalModule,
    postsModule,
    categoriesModule,
    photoGalleryModule
  },
  state: {
    menu: [],
    loadingState: false,
    query: '',
    searchResults: [],
    headerLinks: []
  },
  actions: {
    setHeader ({commit}, postOrPosts) {
      if (postOrPosts) {
        commit(
          'SET_HEADER',
          postOrPosts instanceof Array ? getImagesForPosts(postOrPosts) : getImagesForPost(postOrPosts)
        )
      }
    },
    dropHeader ({commit}) {
      commit('SET_HEADER', undefined)
    },
    get ({state}, params) {
      state.loadingState = true
      return axios.get(...params)
        .then((data) => {
          state.loadingState = false
          return data
        })
        .catch((error) => {
          state.loadingState = false
          throw error
        })
    },
    post ({state}, params) {
      state.loadingState = true
      return axios.post(...params)
        .then((data) => {
          state.loadingState = false
          return data
        })
        .catch((error) => {
          state.loadingState = false
          throw error
        })
    },
    requestMenu ({commit, dispatch}) {
      return dispatch('get', ['/wp-json/myroutes/main_menu'])
        .then(({data}) => commit('SET_MENU', data.map((item) => ({
          ...item,
          url: item.url.replace(process.env.baseUrl, '')
        }))))
    },
    requestForSearch ({commit, dispatch}, query) {
      commit('SET_QUERY', query)
      return dispatch('get', [`/wp-json/wp/v2/country?search=${query}`])
        .then(({data}) => commit('SET_SEARCH_RESULTS', data))
    },
    requestItemBySlug ({commit, dispatch}, {name, slug}) {
      return dispatch('get', [`/wp-json/wp/v2/${name}`, {params: {slug}}])
        .then(({data}) => data[0])
    },
    requestItemsByType ({commit, dispatch}, {name}) {
      return dispatch('get', [`/wp-json/wp/v2/${name}?_embed=true&per_page=998`])
    },
    async initialRequest ({dispatch}) {
      return Promise.all([
        dispatch('countriesModule/init'),
        dispatch('requestMenu'),
        dispatch('postsModule/requestPosts'),
        dispatch('toursModule/requestTours'),
        dispatch('hotelsModule/requestHotels'),
        dispatch('categoriesModule/requestCategories'),
        dispatch('photoGalleryModule/requestPhotoGalleries')
      ])
    }
  },
  mutations: {
    SET_MENU (state, data) {
      state.menu = data
    },
    SET_QUERY (state, query) {
      state.query = query
    },
    SET_SEARCH_RESULTS (state, searchResults) {
      state.searchResults = searchResults
    },
    SET_HEADER (state, urls) {
      state.headerLinks = urls
    }
  }
})
