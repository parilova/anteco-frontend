export default {
  state: {
    media: []
  },
  getters: {
    getPhotoGalleriesById: ({media}) => (searchId) => {
      return media.find(({id}) => id === searchId)
    }
  },
  actions: {
    requestPhotoGalleries: async ({commit, dispatch}) => {
      const gettedPhotos = await dispatch('requestItemsByType', {name: 'photos'}, {root: true})
      const result = gettedPhotos.data.map((item) => ({
        ...item,
        images: item.content.rendered.match(/(src="([^"]*)")/gi).map((s) => s.replace('src="', '').replace('"', ''))
      }))
      commit('SET_PHOTO_GALLERIES', result)
    }
  },
  mutations: {
    SET_PHOTO_GALLERIES: (state, media) => {
      state.media = media
    }
  },
  namespaced: true
}
