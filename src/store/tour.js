// import {CATEGORIES} from '../constants'

export default {
  state: {
    tours: []
  },
  getters: {
    getToursByCountryId: ({tours}) => (countryId) => {
      return tours
        .filter((tour) => tour.acf && tour.acf.country && tour.acf.country.ID === countryId)
    },
    getTourBySlug: ({tours}) => (slugToCompare) => {
      return tours.find(({slug}) => slug === slugToCompare)
    }
  },
  actions: {
    requestTours: async ({commit, dispatch}) => {
      const tours = await dispatch('get', ['/wp-json/wp/v2/tour'], {root: true})
        .then(({data}) => data)
      commit('SET_TOURS', tours)
    }
  },
  mutations: {
    SET_TOURS: (state, tours) => {
      state.tours = tours
    }
  },
  namespaced: true
}
