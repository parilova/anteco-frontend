export default {
  state: {
    categories: []
  },
  getters: {
    getCategoryBySlug: (state) => (slug) => {
      return state.categories.find((x) => x.slug === slug)
    }
  },
  actions: {
    requestCategories ({commit, dispatch}) {
      return dispatch('requestItemsByType', {name: 'categories'}, {root: true})
        .then(({data}) => commit('SET_CATEGORIES', data))
    }
  },
  mutations: {
    SET_CATEGORIES (state, data) {
      state.categories = data
    }
  },
  namespaced: true
}
