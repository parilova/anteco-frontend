import { CATEGORIES } from '../constants'
export default {
  state: {
    posts: []
  },
  getters: {
    getNews (state) {
      return state.posts.filter((x) => x.categories.indexOf(CATEGORIES.NEWS) > -1)
    },
    getSpecialOffers (state) {
      return state.posts.filter((x) => x.categories.indexOf(CATEGORIES.SPECIAL_OFFERS) > -1)
    },
    getEvents (state) {
      return state.posts.filter((x) => x.categories.indexOf(CATEGORIES.EVENTS) > -1)
    },
    getLastFiveNews (state, getters) {
      return getters.getNews.slice(0, 5)
    },
    getLastThreeSpecialOffers (state, getters) {
      return getters.getSpecialOffers.slice(0, 3)
    },
    getLastThreeEvents (state, getters) {
      return getters.getEvents.slice(0, 3)
    }
  },
  actions: {
    requestPosts ({commit, dispatch}) {
      return dispatch('requestItemsByType', {name: 'posts'}, {root: true})
        .then(({data}) => commit('SET_POSTS', data))
    }
  },
  mutations: {
    SET_POSTS (state, data) {
      state.posts = [...state.posts, ...data]
    }
  },
  namespaced: true
}
