export default {
  state: {
    active: false,
    sended: false
  },
  actions: {
    toggle ({commit, state}) {
      commit('SET_ACTION', !state.active)
    },
    async send ({dispatch, commit}, email) {
      try {
        const bodyFormData = new FormData()
        bodyFormData.set('user_email', email)
        bodyFormData.set('user_login', email.split('@')[0])
        bodyFormData.set('wp-submit', 'Регистрация')
        await dispatch('post', ['/wp-login.php?action=register', bodyFormData], {root: true})
      } catch (e) {
        console.error(e)
      }
      commit('SENDED')
      commit('SET_ACTION', false)
    }
  },
  mutations: {
    SET_ACTION (state, newState) {
      state.active = newState
    },
    SENDED (state) {
      state.sended = true
    }
  },
  namespaced: true
}
