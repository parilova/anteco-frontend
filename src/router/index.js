import Vue from 'vue'
import Router from 'vue-router'

import MainPage from '@/pages/MainPage'

import NewsPage from '@/pages/NewsPage'
import NewsArticle from '@/pages/NewsArticle'

import EventsPage from '@/pages/EventsPage'
import EventPage from '@/pages/EventPage'

import SpecialOffersPage from '@/pages/SpecialOffersPage'
import SpecialOfferPage from '@/pages/SpecialOfferPage'

import CountryPage from '@/pages/CountryPage'
import RegionsPage from '@/pages/RegionsPage'
import TourPage from '@/pages/TourPage'
import MainCountries from '@/pages/MainCountries'

import PublicationPage from '@/pages/PublicationPage'
import Photos from '@/pages/Photos'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'MainPage',
      component: MainPage
    },
    {
      path: '/category/specpredlozheniya',
      name: 'SpecialOffersPage',
      component: SpecialOffersPage
    },
    {
      path: '/specpredlozheniya/:id',
      name: 'SpecialOfferPage',
      component: SpecialOfferPage
    },
    {
      path: '/category/novosti',
      name: 'NewsPage',
      component: NewsPage
    },
    {
      path: '/novosti/:id',
      name: 'NewsArticle',
      component: NewsArticle
    },
    {
      path: '/sobytiya',
      name: 'EventsPage',
      component: EventsPage
    },
    {
      path: '/sobytiya/:id',
      name: 'EventPage',
      component: EventPage
    },
    {
      path: '/strany',
      name: 'MainCountries',
      component: MainCountries
    },
    {
      path: '/continent/:continent_slug/',
      name: 'RegionsPage',
      component: RegionsPage
    },
    {
      path: '/country/:country_slug',
      name: 'CountryPage',
      component: CountryPage
    },
    {
      path: '/tour/:tour_id',
      name: 'TourPage',
      component: TourPage
    },
    {
      path: '/:id',
      name: 'Publication',
      component: PublicationPage
    },
    {
      path: '/photos/:slug',
      name: 'Photos',
      component: Photos
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
