// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import moment from 'vue-moment'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VueYandexMetrika from 'vue-yandex-metrika'
import 'swiper/dist/css/swiper.css'

Vue.use(moment)
Vue.use(VueAwesomeSwiper)

Vue.config.productionTip = false
/* eslint-disable no-new */
Vue.use(VueYandexMetrika, {
  id: '48284186',
  router,
  env: process.env.NODE_ENV
  // other options
})
router.beforeEach((to, from, next) => {
  store.dispatch('dropHeader')
  next()
})
new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>'
})
